'use strict'

var fs = require('fs');
var readline = require('readline');

// use this to parse the XML file
var Saxophonist = require('saxophonist')

// pass any numbers of files as arguments when you launch this
// Usage: node stressor file1 file2
// Example: node stressor wikipedia/enwiki-20151201-pages-meta-current1.xml-p000000010p000010000
// Example: node stressor wikipedia/*
var files = process.argv.splice(2)

// use pump to pipe the streams
// var pump = require('pump') // I used Promise.all instead

parse();

function parse() {

  console.time('parsing time');
  
  console.log('parsing', files);

  Promise.all(files.map(file =>
    new Promise((res, rej) => {
      var total = 0;
      var saxo = new Saxophonist('page');
      saxo.on('data', el => {
        total++;
      });
      saxo.on('end', () => res(total));
      saxo.on('error', e => rej(e));

      fs.createReadStream(file).pipe(saxo);
    })
  )).then(counts => {
    console.timeEnd('parsing time');
    console.log('pages', counts.reduce((a,b)=>a+b));
  })
  .catch(console.error)
}



// quick and dirty (unsafe, CDATA etc..) to have an idea of what the fastest can be
function parseBasic() {

  console.time('parsing time');
  
  console.log('parsing', files);

  Promise.all(files.map(file =>
    new Promise((res, rej) => {
      var total = 0;
      readline.createInterface({input: fs.createReadStream(file)})
      .on('line', l => {
        total += l.includes('<page>'); // or l.match(/^\s*<page>/) ? 1 : 0
      })
      .on('close', () => {
        res(total);
      })
      .on('error', e => rej(e));
    })
  )).then(counts => {
    console.timeEnd('parsing time');
    console.log('pages', counts.reduce((a,b)=>a+b));
  })
  .catch(console.error)
}
