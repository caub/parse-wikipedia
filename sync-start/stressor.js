'use strict'

var fs = require('fs')

// use this to parse the XML file
var parseString = require('xml2js').parseString

// pass any numbers of files as arguments when you launch this
// Usage: node stressor file1 file2
// Example: node stressor wikipedia/enwiki-20151201-pages-meta-current1.xml-p000000010p000010000
// Example: node stressor wikipedia/*
var files = process.argv.splice(2)

// to display the time taken to process all files
console.time('parsing time')

parse();

function parse (err) {
  console.log('parsing', files);

  Promise.all(files.map(file =>
    new Promise((res, rej) => {
      fs.readFile(file, function (err, data) {
        if (err) return rej(err);

        parseString(data, function (err, result) {
          if (err) return rej(err);

          console.log(process.memoryUsage()); // should be measured during execution, not after
          
          res(result.mediawiki.page.length)
        });
      })
    })
  )).then(counts => {
    console.timeEnd('parsing time');
    console.log(counts.reduce((a,b)=>a+b))
  })
  .catch(console.error)
}
